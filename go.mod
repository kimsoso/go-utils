module gitee.com/kimsoso/go-utils

go 1.18

require (
	github.com/fsnotify/fsnotify v1.5.1
	github.com/vmihailenco/msgpack/v5 v5.3.5
)

require (
	github.com/vmihailenco/tagparser/v2 v2.0.0 // indirect
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c // indirect
)
