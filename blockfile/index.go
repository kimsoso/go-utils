/*
 * @Author: soso
 * @Date: 2022-02-18 17:15:45
 * @LastEditTime: 2022-02-18 17:17:01
 * @LastEditors: Please set LastEditors
 * @Description: 索引数据块
 * @FilePath: /go-utils/blockfile/index.go
 */
package blockfile

type bindex struct {
	key    []int // 可能存在一次写入多条数据的情况
	offset uint64
	size   uint16
}

func (bi *bindex) Key() []int {
	return bi.key
}
func (bi *bindex) Offset() uint64 {
	return bi.offset
}
func (bi *bindex) Size() uint16 {
	return bi.size
}
