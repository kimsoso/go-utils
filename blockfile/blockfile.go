package blockfile

type Blockfile struct {
	path   string
	index  *blockindex
	blocks [][]byte
}

func NewBlockfile() *Blockfile {
	return &Blockfile{}
}

func (bf *Blockfile) Add(data []byte) (key int, err error) {
	return 0, nil
}

func (bf *Blockfile) Adds(datas [][]byte) (keys []int, err error) {
	return []int{}, nil
}

func (bf *Blockfile) Set(key int, data []byte) (err error) {
	return nil
}

func (bf *Blockfile) Get(key int) (out []byte, err error) {
	return nil, nil
}

func (bf *Blockfile) GetsFrom(keyFrom int) (out []byte, err error) {
	return nil, nil
}

func (bf *Blockfile) Clean() (err error) {
	return nil
}

func (bf *Blockfile) load() (err error) {
	return nil
}
