/*
 * @Author: soso
 * @Date: 2022-02-11 18:10:04
 * @LastEditTime: 2022-02-18 17:15:12
 * @LastEditors: Please set LastEditors
 * @Description: 块文件的索引
 * @FilePath: /go-mesh-cli/utils/blockfile/indexfile.go
 */

package blockfile

type blockindex struct {
	path    string
	keysMap map[int]int //map[key]块索引 key=>index of keys
	keys    []*bindex
}

func (bi *blockindex) Add(key int, offset uint64, size uint16) (err error) {
	return nil
}

func (bi *blockindex) Adds(keys []int, offset uint64, size uint16) (err error) {
	return nil
}

func (bi *blockindex) Set(key int, offset uint64, size uint16) (err error) {
	return nil
}

func (bi *blockindex) Sets(keys []int, offset uint64, size uint16) (err error) {
	return nil
}

func (bi *blockindex) Get(key int) (data *bindex, err error) {
	return nil, nil
}

func (bi *blockindex) Gets(keys []int) (data *bindex, err error) {
	return nil, nil
}

func (bi *blockindex) Clean() (err error) {
	return nil
}

func (bi *blockindex) load() (err error) {
	return nil
}
