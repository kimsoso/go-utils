//go:build !windows

package sofile

import (
	"os"
	"time"
)

// Set the access, creation, and modification times of a file.
func SetFileTime(path string, atime, ctime, mtime time.Time) (err error) {
	return os.Chtimes(path, atime, mtime)
}

// import (
// 	"errors"
// 	"gomeshsync/utils/utils"
// 	"io/ioutil"
// 	"os"

// 	"gopkg.in/yaml.v3"
// )

// type BlockFile struct {
// 	Path   string                  // path to main storage
// 	Bfi    *blockIndexFile         // index of blocks
// 	Blocks map[string]*interface{} // map of block
// }

// func NewBlockFile(path string) *BlockFile {
// 	bf := &BlockFile{Path: path}
// 	bf.Bfi = NewIndexFile(bf.getIdxFilePath())
// 	bf.Blocks = map[string]*interface{}{}
// 	if _, err := os.Stat(bf.Path); os.IsNotExist(err) {
// 		bf.createFile()
// 	}
// 	bf.load()
// 	return bf
// }

// func (bf *BlockFile) getIdxFilePath() string {
// 	return bf.Path + ".idx"
// }

// func (bf *BlockFile) GetBlocks() map[string]*interface{} {
// 	return bf.Blocks
// }

// func (bf *BlockFile) createFile() error {
// 	f, err := os.Create(bf.Path)
// 	if err != nil {
// 		return err
// 	}
// 	defer f.Close()
// 	return nil
// }

// func (bf *BlockFile) load() error {
// 	buf, _ := ioutil.ReadFile(bf.Path)
// 	if len(buf) > 0 {
// 		return yaml.Unmarshal(buf, &bf.Blocks)
// 	}
// 	return nil
// }

// func (bf *BlockFile) Add(key string, block interface{}) error {
// 	if _, ok := bf.Blocks[key]; ok {
// 		return errors.New("key:" + key + " already exist")
// 	}
// 	bbf := map[string]*interface{}{}
// 	bbf[key] = &block
// 	buf, _ := yaml.Marshal(bbf)

// 	if err := utils.AppendFile(bf.Path, buf); err != nil {
// 		return err
// 	}

// 	if err := bf.Bfi.add(key, int64(len(buf))); err != nil {
// 		return err
// 	}

// 	bf.Blocks[key] = &block
// 	return nil
// }

// func (bf *BlockFile) Delete(key string) error {
// 	if _, ok := bf.Blocks[key]; !ok {
// 		return errors.New("key:" + key + " is not exist")
// 	}

// 	bfi := bf.Bfi.getBlock(key)

// 	if err := utils.FileCropBlock(bf.Path, bfi.Offset, bfi.Size); err == nil {
// 		if err := bf.Bfi.delete(key); err != nil {
// 			return err
// 		}

// 		delete(bf.Blocks, key)
// 		return nil
// 	} else {
// 		return err
// 	}
// }
// func (bf *BlockFile) Get(key string) *interface{} {
// 	return bf.Blocks[key]
// }

// func (bf *BlockFile) Set(key string, block interface{}) error {
// 	if _, ok := bf.Blocks[key]; !ok {
// 		return errors.New("key:" + key + " is not exist")
// 	}
// 	blocks := map[string]*interface{}{}
// 	blocks[key] = &block

// 	blockBuf, err := yaml.Marshal(blocks)
// 	if err != nil {
// 		return err
// 	}

// 	bfi := bf.Bfi.getBlock(key)

// 	if bfi.Size == int64(len(blockBuf)) {
// 		return utils.FileReplaceBlock(bf.Path, bfi.Offset, bfi.Size, blockBuf)
// 	} else {
// 		leftBuf, err := utils.FileReadToEnd(bf.Path, bfi.Offset+bfi.Size)
// 		if err != nil {
// 			return err
// 		}

// 		os.Truncate(bf.Path, bfi.Offset)

// 		err = utils.AppendFile(bf.Path, blockBuf)
// 		if err != nil {
// 			return err
// 		}

// 		err = utils.AppendFile(bf.Path, leftBuf)
// 		if err != nil {
// 			return err
// 		}

// 		err = bf.Bfi.set(key, int64(len(blockBuf)))
// 		if err != nil {
// 			return err
// 		}
// 	}
// 	bf.Blocks[key] = &block
// 	return nil
// }

// func (bf *BlockFile) Destroy() {
// 	os.RemoveAll(bf.Path)
// 	bf.Blocks = map[string]*interface{}{}
// 	bf.Bfi.destroy()
// }
