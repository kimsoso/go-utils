package soslice

import (
	"fmt"
)

type Orderable interface {
	int | int8 | int16 | int32 | int64 |
		uint | uint8 | uint16 | uint32 | uint64 | uintptr |
		float32 | float64 |
		string
}

// 求最大值
func Max[T Orderable](in []T) (out T) {
	out = in[0]
	for _, v := range in {
		if v > out {
			out = v
		}
	}
	return
}

// 求最小值
func Min[T Orderable](in []T) (out T) {
	out = in[0]
	for _, v := range in {
		if v < out {
			out = v
		}
	}
	return
}

// 切片分段最小值
func MinRanges[T Orderable](in []T, count ...int) (out map[int]T) {
	maxLen := Max(count)
	if maxLen > len(in) {
		panic("MaxRanges: count parameter exceed in range!")
	}

	out = make(map[int]T, len(count))
	min := in[0]

	for i := 1; i < maxLen; i++ {
		if in[i] < min {
			min = in[i]
		}
		for _, c := range count {
			if i < c {
				out[c] = min
			}
		}
	}
	return
}

// 切片分段最大值
func MaxRanges[T Orderable](in []T, count ...int) (out map[int]T) {
	maxLen := Max(count)
	if maxLen > len(in) {
		panic("MaxRanges: count parameter exceed in range!")
	}

	out = make(map[int]T, len(count))
	max := in[0]

	for i := 1; i < maxLen; i++ {
		if in[i] > max {
			max = in[i]
		}
		for _, c := range count {
			if i < c {
				out[c] = max
			}
		}
	}
	return
}

// 翻转切片
func Reverse[T Orderable](in []T) {
	for i, j := 0, len(in)-1; i < j; i, j = i+1, j-1 {
		fmt.Println(i, j)
		in[i], in[j] = in[j], in[i]
	}
}
