//go:build windows

/*
 * @Author: soso
 * @Date: 2022-01-24 13:04:50
 * @LastEditTime: 2022-01-25 10:47:51
 * @LastEditors: Please set LastEditors
 * @Description: windows相关文件操作
 * @FilePath: /sync-client/utils/file_win.go
 */

package utils

import (
	"os"
	"reflect"
	"strconv"
	"syscall"
)

func HideFile(filename string) error {
	filenameW, err := syscall.UTF16PtrFromString(filename)
	if err != nil {
		return err
	}
	err = syscall.SetFileAttributes(filenameW, syscall.FILE_ATTRIBUTE_HIDDEN)
	if err != nil {
		return err
	}
	return nil
}

func CheckIsHidden(file os.FileInfo) bool {
	fa := reflect.ValueOf(file.Sys()).Elem().FieldByName("FileAttributes").Uint()
	bytefa := []byte(strconv.FormatUint(fa, 2))
	if bytefa[len(bytefa)-2] == '1' {
		return true
	}
	return false
}
