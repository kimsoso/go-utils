//go:build !windows

/*
 * @Author: soso
 * @Date: 2022-01-24 13:04:44
 * @LastEditTime: 2022-01-24 15:45:50
 * @LastEditors: Please set LastEditors
 * @Description: 针对类unix系统操作
 * @FilePath: /sync-client/utils/file_unix.go
 */

package utils

import (
	"os"
	"path/filepath"
	"strings"
)

func HideFile(filename string) error {
	if !strings.HasPrefix(filepath.Base(filename), ".") {
		err := os.Rename(filename, "."+filename)
		if err != nil {
			return err
		}
	}
	return nil
}

func CheckIsHidden(file os.FileInfo) bool {
	return file.Name()[0:1] == "."
}
