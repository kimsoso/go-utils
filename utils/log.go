/*
 * @Author: soso
 * @Date: 2022-01-25 14:45:08
 * @LastEditors: Please set LastEditors
 * @Description: 日志相关操作
 * @FilePath: /go-mesh-sync/go-utils/utils/log.go
 */

package utils

// func LPN(v ...interface{}) {
// 	log.Print("[", MyCaller(), "]")
// 	log.Println(v...)
// }
// func FPN(v ...interface{}) {
// 	fmt.Print("[", MyCaller(), "]")
// 	fmt.Println(v...)
// }
// func FPF(fmts string, v ...interface{}) {
// 	fmt.Print("[", MyCaller(), "]")
// 	fmt.Printf(fmts, v...)
// }
