/*
 * @Author: soso
 * @Date: 2022-01-20 18:29:37
 * @LastEditTime: 2022-01-26 14:57:37
 * @LastEditors: Please set LastEditors
 * @Description: soso
 * @FilePath: /file-sync/utils/time.go
 */
package utils

import "time"

var (
	location *time.Location
)

func init() {
	location, _ = time.LoadLocation("Asia/Shanghai")
}

func TimeFromString(val string) time.Time {
	tm, _ := time.ParseInLocation("2006-01-02 15:04:05", val, location)
	return tm
}

func LastSecOfTheDayBeforeYesterday(tm time.Time) time.Time {
	tm = tm.Add(-time.Hour * 48)
	return TimeFromString(tm.Format("2006-01-02") + " 23:59:59")
}
